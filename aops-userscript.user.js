// ==UserScript==
// @name         HC's Custom AoPS UserScript
// @author       happycupcake/epiccakeking
// @description  Custom AoPS UserScript.
// @version      1.1.1
// @namespace    https://gitlab.com/epiccakeking
// @match        https://artofproblemsolving.com/*
// @run-at       document-start
// @grant        none
// @license      MIT
// ==/UserScript==

//Add custom stylesheet
document.documentElement.appendChild(document.createElement('style')).textContent = `
#feed-topic .cmty-topic-moderate{
  display: inline !important;
}
#feed-wrapper .aops-scroll-inner{
  overscroll-behavior: contain;
}
.cmty-post .cmty-post-username{ display: none !important; }

.cmty-post .cmty-phone-poster{
  display: inline-block;
  float: left;
  font-size: 10pt;
	line-height: 12pt;
	margin-right: 5px;
}
.cmty-post-top{
	display: contents;
}
.cmty-post .cmty-avatar{
  height: 40px;
  width: 40px;
}
.cmty-post-left{
  width: 50px !important;
  padding: 5px;
}
.cmty-post-num-posts{
  display: none !important;
}
.cmty-post-html{
	margin-right: -50px !important;
}

#top-bar{
	display: none;
}
#header{
	margin: 0px !important;
}
#small-footer-wrapper{
	display: none !important;
}
`;

// Wait until document is ready.
document.addEventListener('DOMContentLoaded', () => {
  // Safety first!
  if (AoPS.Community) {
    // Better quotes
    AoPS.Community.Views.Post.prototype.onClickQuote = function (e) {
      if (e.ctrlKey) {
        this.topic.appendToReply(`@[url=https://aops.com/community/p${this.model.get("post_id")}]${this.model.get("username")} (#${this.model.get("post_number")}):[/url]`);
      } else {
        this.topic.appendToReply(`[quote name="${this.model.get("username")}" url="/community/p${this.model.get("post_id")}"]
${this.model.get("post_canonical").trim()}
[/quote]

`);
      }
    }

    // Copy links
    AoPS.Community.Views.Post.prototype.onClickDirectLink = function (e) {
      navigator.clipboard.writeText('https://aops.com/community/p' + this.model.get("post_id"));
      AoPS.Ui.Flyout.display("Url copied (https://aops.com/community/p" + this.model.get("post_id") + ").");
    }

    //Notifications
    Notification.requestPermission().then((permission) => {
      if (permission == "granted") {
        AoPS.Ui.Flyout.display = function (x) {
          var textextract = document.createElement("div")
          textextract.innerHTML = x.replace('<br>', '\n');
          var y = $(textextract).text()
          var notification = new Notification("AoPS", { body: y, icon: 'https://artofproblemsolving.com/online-favicon.ico', tag: y });
          setTimeout(notification.close.bind(notification), 4000);
        }
      }
    });
    //Change post deleted action
    AoPS.Community.Views.Post.prototype.removePostFromTopic = AoPS.Community.Views.Post.prototype.setVisibility;

    //Allow editing in locked topics
    AoPS.Community.Views.Post.prototype["render"] = new Function(
      "a",
      AoPS.Community.Views.Post.prototype["render"].toString()
        .replace(/^function[^{]+{/i, "var e=AoPS.Community.Lang;")
        .replace("can_edit:", "can_edit: this.topic.model.attributes.permissions.c_can_edit ||")
        .replace(/}[^}]*$/i, "")
    );
  }
});
